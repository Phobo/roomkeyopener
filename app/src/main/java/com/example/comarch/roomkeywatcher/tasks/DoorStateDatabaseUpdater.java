package com.example.comarch.roomkeywatcher.tasks;

import android.os.AsyncTask;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DoorStateDatabaseUpdater extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {

        final String date = params[0];
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("server/saving-data/DoorOpened");
        ref.push().setValue(date);

        return "Executed";
    }

    @Override
    protected void onPostExecute(String result) {

    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }
}
