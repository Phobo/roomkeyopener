package com.example.comarch.roomkeywatcher.listeners;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.ToggleButton;

import com.example.comarch.roomkeywatcher.MainActivity;
import com.example.comarch.roomkeywatcher.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class DoorStateDatabaseListener implements ChildEventListener {

    MainActivity mainActivity;

    public DoorStateDatabaseListener(MainActivity mainActivity) {
        this.mainActivity=mainActivity;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

        final String formattedDate = getCurrentFormattedDate();

        if(dataSnapshot.getValue().equals(formattedDate)){
            ((ToggleButton)mainActivity.findViewById(R.id.doorStateToggleButton)).setChecked(true);
            if (!mainActivity.isAppRunning("com.example.comarch.roomkeywatcher")) {
                showNotification();
            }
        }
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }

    private String getCurrentFormattedDate() {
        Date curentDate =  Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        return dateFormat.format(curentDate);
    }

    private void showNotification() {
        NotificationManager notificationManager =
                (NotificationManager) mainActivity.getSystemService(Activity.NOTIFICATION_SERVICE);

        Context context = mainActivity.getApplicationContext();
        CharSequence contentTitle = "Room Key Watcher";
        CharSequence contentText = "Drzwi otwarte!";
        Intent notificationIntent = new Intent(mainActivity.getApplicationContext(), MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(mainActivity.getApplicationContext(), 0, notificationIntent, 0);

        Notification notification = new Notification.Builder(context)
                .setContentIntent(contentIntent)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setSmallIcon(R.drawable.ic_office)
                .build();

        int SERVER_DATA_RECEIVED = 1;
        notificationManager.notify(SERVER_DATA_RECEIVED, notification);
    }
}


