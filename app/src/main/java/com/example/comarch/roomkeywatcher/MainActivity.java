package com.example.comarch.roomkeywatcher;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.comarch.roomkeywatcher.listeners.DoorStateDatabaseListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        checkDoorStateInDatabase();
    }

    private void checkDoorStateInDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference databaseReference = database.getReference("server/saving-data/DoorOpened");
        databaseReference.addChildEventListener(new DoorStateDatabaseListener(this));
    }

    public void openQrActivity(View view) {
        Intent myIntent = new Intent(MainActivity.this, CameraQRActivity.class);
        myIntent.putExtra("key", "value"); //Optional parameters
        MainActivity.this.startActivity(myIntent);
    }

    public boolean isAppRunning(final String packageName) {
        final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        if (procInfos != null)
        {
            for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                if (processInfo.processName.equals(packageName)) {
                    return true;
                }
            }
        }
        return false;
    }
}
