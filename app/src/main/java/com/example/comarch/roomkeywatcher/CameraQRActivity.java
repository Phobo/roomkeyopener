package com.example.comarch.roomkeywatcher;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.comarch.roomkeywatcher.tasks.DoorStateDatabaseUpdater;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Grzesiek on 03.03.2018.
 */

public class CameraQRActivity extends Activity {

    private static final int MY_CAMERA_REQUEST_CODE = 100;
    public static final String PREF_BARCODE_INFO = "barcodeInfo";
    public static final String PREF_NOT_SENT_YET = "notSentYet";

    private SurfaceView cameraView;
    private TextView barcodeInfo;

    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private SharedPreferences preferences;
    private boolean notSentYet = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.camera_qr);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        ActivityCompat.requestPermissions(CameraQRActivity.this, new
                String[]{android.Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);

        cameraView = findViewById(R.id.camera_view);
        barcodeInfo = findViewById(R.id.barcode_info);

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector).build();

        cameraView.getHolder().addCallback(new CameraViewCallback());
        barcodeDetector.setProcessor(new DetectorProcessor());
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_BARCODE_INFO, String.valueOf(barcodeInfo.getText()));
        editor.putBoolean(PREF_NOT_SENT_YET, notSentYet);
        editor.apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(PREF_BARCODE_INFO, "");
        editor.putBoolean(PREF_NOT_SENT_YET, true); //TODO change when checking state from base
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        barcodeInfo.setText(preferences.getString(PREF_BARCODE_INFO, ""));
        notSentYet = preferences.getBoolean(PREF_NOT_SENT_YET, true);
    }

    private void showToast(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private void updateDoorStateOnDatabase() {
        String formattedDate = getCurrentFormattedDate();
        new DoorStateDatabaseUpdater().execute(formattedDate, "opened");
    }

    private String getCurrentFormattedDate() {
        Date curentDate =  Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        return dateFormat.format(curentDate);
    }

    private class DetectorProcessor implements Detector.Processor {

        @Override
        public void release() {
        }

        @Override
        public void receiveDetections(Detector.Detections detections) {
            final SparseArray barcodes = detections.getDetectedItems();

            if (barcodes.size() != 0) {
                barcodeInfo.post(new Runnable() {
                    @Override
                    public void run() {
                        String detectedValue = ((Barcode) barcodes.valueAt(0)).displayValue;
                        barcodeInfo.setText(detectedValue);
                        if (notSentYet && detectedValue != null && !detectedValue.isEmpty()) {
                            updateDoorStateOnDatabase();
                            notSentYet = false;
                            showToast("Wysłano powiadomienie");
                        }
                    }
                });
            }
        }
    }

    private class CameraViewCallback implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                if (ActivityCompat.checkSelfPermission(
                        CameraQRActivity.this.getApplicationContext(),
                        android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                cameraSource.start(cameraView.getHolder());
            } catch (IOException ie) {
                Log.e("CAMERA SOURCE", ie.getMessage());
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            cameraSource.stop();
        }
    }

}
